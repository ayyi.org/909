# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

inherit autotools

RESTRICT="nomirror"
IUSE=""
DESCRIPTION=""
HOMEPAGE="http://ayyi.org/"
SRC_URI="http://ayyi.org/files/${P}.tar.xz"

LICENSE="GPL-3"
KEYWORDS="x86 amd64"
SLOT="0"

DEPEND=">=media-sound/jack-audio-connection-kit-0.99
	>=media-libs/libsndfile-1.0.10
	>=x11-libs/gtk+-2.6
	sys-apps/dbus
	dev-libs/dbus-glib"

src_compile() {
	local myconf

	econf \
		${myconf} \
		|| die
	emake || die
}

src_install() {
	make DESTDIR=${D} install || die
	dodoc NEWS ChangeLog
}

