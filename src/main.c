/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <sys/time.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "model/ayyi_model.h"

typedef enum
{
	KIK = 0,
	SNR,
	HAT1,
	HAT2,
} File;

typedef AyyiCallback Fn;

char wav_files[][64] = {
	"909_1_kik.wav", "909_2_snr.wav", "909_3_hat.wav", "909_4_hat.wav"
};

char names[][64] = {
	"Kick", "Snr", "Hat", "Hat"
};

static PoolItem*  new_pool_items[3] = {NULL,}; // newly created pool items for each of the wav files.
static AyyiTrack* new_tracks    [3] = {NULL,};

int          debug = 0;
bool         remove_files = false;
GMainLoop*   loop = NULL;

static void  log_handler       (const gchar*, GLogLevelFlags, const gchar*, gpointer);
static void  import_files      (AyyiCallback);
static void  add_tracks        (AyyiCallback);
static void  add_patterns      (AyyiCallback);
static void  remove_from_song  (AyyiCallback);
static void  quit              ();

// consider restoring these two functions to model/utils.c
static void am_do_series  (Fn* functions, AyyiCallback finished, gpointer user_data);
static void am_do_series_ (Fn functions[], int n_functions, AyyiCallback finished);


int
print_help ()
{
	printf("Usage: " PACKAGE_STRING "\n"
	     "  Add 909 drum machine samples to the current song.\n\n"
	     "  -r, --remove                     Remove the 909 samples from the current song and any parts that use them\n"
	     "  -v, --version                    Show version information\n"
	     "  -h, --help                       Print this message\n"
	     "  -d, --debug     level            Output debug info to stdout\n"
		);
	return 1;
}


int
parse_opts (int argc, char* argv[])
{
	const char* optstring = "hvrd:";

	const struct option longopts[] = {
		{ "remove", 0, 0, 'r' },
		{ "version", 0, 0, 'v' },
		{ "help", 0, 0, 'h' },
		{ "debug", 1, 0, 'd' },
		{ 0, 0, 0, 0 }
	};

	int option_index = 0;
	int c = 0;

	while (1) {
		c = getopt_long (argc, argv, optstring, longopts, &option_index);

		if (c == -1) {
			break;
		}

		switch (c) {
		case 0:
			break;

		case 'r':
			remove_files = true;
			break;

		case 'v':
			printf("version " PACKAGE_VERSION "\n");
			exit (0);
			break;

		case 'h':
			print_help ();
			exit (0);
			break;
		case 'd':
			debug = atoi(optarg);
			break;

		default:
			return print_help();
		}
	}

	if (optind < argc) {
		//session_name = argv[optind++];
	}

	return 0;
}


int
main (int argc, char** argv)
{
	printf("%s%s. Version %s                                 %s\n", green_r, PACKAGE_NAME, VERSION, white);

	g_log_set_handler (NULL, G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gtk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gdk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib-GObject", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gdl", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Ayyi", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);

	parse_opts (argc, argv);

	gtk_init(&argc, &argv);

	am__init(NULL);

	if(false) _debug_ = 1;

	void
	on_shm (GError* e, gpointer user_data)
	{
		if (!ayyi.got_shm) return;

		if(e){
			printf("failed to connect\n");
			if(loop){
				g_main_loop_unref(loop);
				loop = NULL;
			}
			exit(EXIT_FAILURE);
		}

		if(remove_files){
			void remove_finished (gpointer user_data)
			{
				exit(EXIT_SUCCESS);
			}
			remove_from_song(remove_finished);

			return;
		}

		void import_done (gpointer data)
		{
			PF;

			void add_tracks_done (gpointer data)
			{
				PF;

				void add_patterns_done (gpointer data)
				{
					printf("\n");
					ayyi_log_print(LOG_OK, "909 import complete\n");
					dbg(2, "n_files=%i", ayyi_song__get_file_count());

					exit(EXIT_SUCCESS);
				}

				// 3- add patterns
				add_patterns(add_patterns_done);
			}

			if(new_pool_items[0] && new_pool_items[1]){
				// 2- add tracks
				add_tracks(add_tracks_done);
			}else{
				dbg(0, "failed to import audio. aborting...");
				quit();
				return;
			}
		}

		import_files(import_done);
	}

	am__connect_all(on_shm, NULL);

	g_main_loop_run (loop = g_main_loop_new (NULL, 0));

	return EXIT_SUCCESS;
}


static char*
find_file (const char* filename)
{
	// returned path must be freed.

	#define file_paths PACKAGE_DATA_DIR "/" PACKAGE "/audio/:audio/:../audio/"
	gchar** paths = g_strsplit(file_paths, ":", 0);
	char* path;
	int i = 0;
	while((path = paths[i++])){
		if(!g_file_test(path, G_FILE_TEST_EXISTS)) continue;

		gchar* filepath = g_build_filename(path, filename, NULL);
		if(g_file_test(filepath, G_FILE_TEST_EXISTS)){

			if(filepath[0] != '/'){
				gchar* cwd = g_get_current_dir();
				gchar* abspath = g_build_filename(cwd, filepath, NULL);
				g_free(cwd);
				g_free(filepath);

				dbg(2, "abspath=%s", abspath);
				return abspath;

			} else return filepath;
		}

		g_free(filepath);
	}
	return NULL;
}


static void
import_files (AyyiCallback on_finish)
{
	static int done = 0;

	typedef struct {AyyiCallback finish;} C;
	C* closure = AYYI_NEW(C,
		.finish = on_finish
	);

	void file_import_done(AyyiIdent file_id, GError** error, gpointer user_data)
	{
		PF;
		if(error && (*error)){
			pwarn("%s", (*error)->message);
			quit();
			return;
		}

		PoolItem* pool_item = new_pool_items[done] = am_pool__get_item_from_idx (file_id.idx);
		if(pool_item)
			printf("file import done: %i: %s\n", done, pool_item->leafname);
		else
			pwarn("%i: no pool item! file_idx=%i", done, file_id.idx);

		if(++done >= G_N_ELEMENTS(wav_files)){
			((C*)user_data)->finish(NULL);
		}
	}

	int i; for(i=0;i<G_N_ELEMENTS(wav_files);i++){
		char* path = find_file(wav_files[i]);

		am_song__add_file(path, file_import_done, closure);

		g_free(path);
	}
}


static void
add_tracks (AyyiCallback on_finish)
{
	printf("adding tracks...\n");

	static int done = 0;

	typedef struct {AyyiCallback finish;} C;
	C* closure = AYYI_NEW(C,
		.finish = on_finish
	);

	void
	on_track_create (AyyiIdent obj, GError** error, gpointer user_data)
	{
		g_return_if_fail(error && !(*error));

		AyyiTrack* track = new_tracks[done] = ayyi_song__audio_track_at(obj.idx);
		dbg(1, "idx=%i name='%s' tot_tracks=%i", obj.idx, track ? track->name : NULL, model_collection_length(am_tracks));

		AMTrack* tr = am_track_list_find_by_ident(song->tracks, obj);
		if(tr){
			char truncated[256];
			filename_remove_extension(wav_files[done], truncated);
			dbg(0, "track %i: rename: %s --> %s", obj.idx, tr->name, truncated);
			am_track__set_name(tr, truncated, NULL, NULL);
		}

		if(++done >= G_N_ELEMENTS(wav_files)){
			((C*)user_data)->finish(NULL);
		}
	}

	int i; for(i=0;i<G_N_ELEMENTS(wav_files);i++){
		char* name = g_strdup_printf("%s", wav_files[i]);
		am_song__add_track(TRK_TYPE_AUDIO, name, 1, on_track_create, closure);
		g_free(name);
	}
}


static void
add_part (File file, AyyiSongPos* pos, Fn done, DoClosure* c)
{
	void b (AyyiIdent obj, GError** error, gpointer user_data)
	{
		HandlerData* d = user_data;
		DoClosure* c = d->user_data;

		((void(*)(DoClosure*))d->callback)(c);
	}

	char* name = g_strdup_printf("%s.1", names[file]);

	uint64_t file_len = waveform_get_n_frames((Waveform*)new_pool_items[file]);
	GPos one_beat = {1, 0, 0};
	uint32_t l = pos2samples(&one_beat);
	uint64_t part_len = file_len > l ? ayyi_samples2mu(l) : 0; // ensure the part is not longer than 1 beat.

	am_song__add_part(AYYI_AUDIO, REGION_FULL, new_pool_items[file], new_tracks[file]->shm_idx, pos, part_len, -1, name, 0, b, ayyi_handler_data_new((AyyiHandler)done, c));

	g_free(name);
}


static void
add_patterns (AyyiCallback _on_finish)
{
	static int kicks_done = 0;
	static int snares_done = 0;
	static int hats_done = 0;

	void add_patterns_3_hat(gpointer _c)
	{
		// 8 x hi hats

		DoClosure* c = _c;

		void hat_done(DoClosure* c) { if(++hats_done >= 8) c->next(c); }

		int k; for(k=0;k<8;k++){
			AyyiSongPos pos = {k/2, (k % 2) * AYYI_SUBS_PER_BEAT / 2, 0};

			add_part(HAT1, &pos, (Fn)hat_done, c);
		}
	}

	void add_patterns_2_snr(gpointer _c)
	{
		// 2 x snare drums

		DoClosure* c = _c;

		void snare_done(DoClosure* c) { if(++snares_done >= 2) c->next(c); }

		int k; for(k=0;k<2;k++){
			AyyiSongPos pos = {2 * k + 1, 0, 0};
			add_part(SNR, &pos, (Fn)snare_done, c);
		}
	}

	/*
	void add_patterns_1_kik(Closure* c)
	{
		//4 x kick drums

		void kick_done(Closure* c) { if(++kicks_done >= 4) c->next(c); }

		int k; for(k=2;k<4;k++){
			SongPos pos = {k, 0, 0};
			add_part(KIK, &pos, kick_done, c);
		}
	}
	*/

	void add_kik (gpointer _c)
	{
		AyyiSongPos pos = {kicks_done++, 0, 0};
		add_part(KIK, &pos, ((DoClosure*)_c)->next, _c);
	}

	dbg(0, "%sadding patterns...%s", green, white);

	Fn fns[] = {
		add_kik,
		add_kik,
		add_kik,
		add_kik,
		add_patterns_2_snr,
		add_patterns_3_hat,
	};
	am_do_series_(fns, G_N_ELEMENTS(fns), _on_finish);
}


static void
remove_from_song (AyyiCallback on_finish)
{
	//this can fail if the Song service renamed the files when importing them.
	PF;

	static int removed = 0;
	static int not_removed = 0;
	static void (*finish)(gpointer) = NULL; finish = on_finish;

	void check_complete ()
	{
		if((removed + not_removed) >= G_N_ELEMENTS(wav_files)){
			printf("remove finished. n_removed=%i\n", removed);
			finish(NULL);
		}
	}

	void remove_file_done (AyyiIdent id, GError** error, gpointer user_data)
	{
		PF;
		removed++;
		check_complete();
	}

	for(int i=0;i<G_N_ELEMENTS(wav_files);i++){

		gchar* basename = g_path_get_basename(wav_files[i]);

		PoolItem* item = am_pool__get_item_from_leafname(basename);
		if(item){
			am_song__remove_file(item, remove_file_done, NULL);
		}
		else{
			dbg(0, "file not in song: %s", wav_files[i]);
			not_removed++;
			check_complete();
		}
		g_free(basename);
	}
}


static void
log_handler(const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
{
	switch(log_level){
		case G_LOG_LEVEL_CRITICAL:
			printf("%s %s\n", ayyi_err, message);
			break;
		case G_LOG_LEVEL_WARNING:
			printf("%s %s\n", ayyi_warn, message);
			break;
		default:
			printf("log_handler(): level=%i %s\n", log_level, message);
			break;
	}
}


static void
quit()
{
	gboolean quit (gpointer d){ g_main_loop_quit(loop); return G_SOURCE_REMOVE; }
	g_idle_add(quit, NULL);
}


static void
am_do_series (Fn* functions, AyyiCallback finished, gpointer user_data)
{
	// functions is a null terminated array of function pointers.

	void do_next (DoClosure* c)
	{
		Fn fn = c->functions[c->i];

		if(!fn){
			g_free(c);
			c->finished(c->user_data);
			return;
		}

		c->i++;

		fn(c);
	}

	DoClosure* c = AYYI_NEW(DoClosure,
		.functions = functions,
		.finished = finished,
		.user_data = user_data,
		.next = (Fn)do_next
	);

	do_next(c);
}


static void
am_do_series_ (Fn functions[], int n_functions, AyyiCallback finished)
{
	// like do_series but takes array instead of pointer array.

	typedef struct
	{
		AyyiCallback finished;
	} C;

	C* c = AYYI_NEW(C,
		.finished = finished
	);

	void series_done (gpointer user_data)
	{
		C* c = user_data;
		call(c->finished, NULL);
		g_free(c);
	}

	Fn* fns = g_malloc0(sizeof(Fn) * (n_functions + 1));
	int i; for(i=0;i<n_functions;i++){
		fns[i] = functions[i];
	}
	am_do_series(fns, series_done, c);
}
