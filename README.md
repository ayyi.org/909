Ayyi 909
========

Ayyi 909 is an Ayyi client utility for which adds drum machine
samples and patterns to the current composition. It is intended
to be a productivity tool assisting in the bootstrapping of a
new audio project.

Ayyi 909 is part of the [Ayyi audio production system](https://www.ayyi.org).

As well as being useful in its own right, it serves as an example
for developers on how to interact with the Ayyi composition service.


What does it do?
----------------

The program will add 4 audio files, add three tracks,
and add a basic drum pattern that can then be edited.


Getting started
---------------

To build and run from git:
```
./autogen.sh && ./configure && make && src/ayyi_909
```

If you prefer, a [tarball is available]().

Before using the program you will need to install the composition service
provided by [ardourd](https://www.ayyi.org/download).
