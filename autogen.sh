#!/bin/sh

echo 'Generating configuration files...'

touch NEWS README AUTHORS ChangeLog
libtoolize --automake
aclocal
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf

git submodule update --init --recursive

cd lib/ayyi && ./autogen
